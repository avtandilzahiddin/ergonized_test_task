# import modules
import string, random, requests, json
from twocaptcha import TwoCaptcha

# create account in discord
def register(user_email, username):
	register_url = ('https://discord.com/api/v9/auth/register')

	#create random password
	password_chars = string.digits + string.ascii_letters
	password_temp = random.sample(password_chars, 10)
	password = ''.join(password_temp)

	#create random birthdate < 18
	random_birthday = f'{random.randint(1970, 2003)}-0{random.randint(1,9)}-0{random.randint(1,9)}'

	#solve captcha
	solver = TwoCaptcha('6d2047f498b2f9d6dd6d823a876411dd')
	captcha_key = solver.hcaptcha(sitekey='4c672d35-0701-42b2-88c3-78380b0db560', url='https://discord.com/api/v9/auth/register')
	key = captcha_key['code']
	registration_data = {
		"captcha_key": key,
		"consent": True,
		"date_of_birth": random_birthday,
		"email": user_email,
		'gift_code_sku_id': None,
		"invite": None,
		"password": password,
		"username": username,
		}
	headers = {
		'Content-Type': 'application/json'
	}
	#register in discord
	requests.post(url=register_url, json=registration_data, headers=headers)

	return password

#login to an existing discord account
def login(user_email, password):
	login_url = 'https://discord.com/api/v9/auth/login'

	login_data = {
		"login": user_email,
		"password": password,
		"undelete": False,
		"captcha_key": None,
		"login_source": None,
		"gift_code_sku_id": None
		}

	r = requests.post(url=login_url, json=login_data)
	return json.loads(r.text)['token']



def main():
	# recieve data from user
	user_email = input('Enter email: ')
	username = input('Enter username: ')

	password = register(user_email=user_email, username=username)
	authorization_token = login(user_email=user_email, password=password)
	print(authorization_token)
	return authorization_token


main()
